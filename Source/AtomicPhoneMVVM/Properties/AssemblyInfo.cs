﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("AtomicPhoneMVVM")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Robert MacLean")]
[assembly: AssemblyProduct("AtomicPhoneMVVM")]
[assembly: AssemblyCopyright("Copyright © Robert MacLean 2012")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: System.CLSCompliant(false)]
[assembly: System.Resources.NeutralResourcesLanguage("en")]
[assembly: ComVisible(false)]
[assembly: Guid("dd623e44-e327-437d-8462-00676ccd42f0")]
[assembly: AssemblyVersion("2.0.0.0")]
[assembly: AssemblyFileVersion("2.0.0.0")]
